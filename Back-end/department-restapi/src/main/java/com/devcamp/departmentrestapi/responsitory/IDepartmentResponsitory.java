package com.devcamp.departmentrestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.departmentrestapi.models.Department;

public interface IDepartmentResponsitory extends JpaRepository<Department,Long> {
    
}
