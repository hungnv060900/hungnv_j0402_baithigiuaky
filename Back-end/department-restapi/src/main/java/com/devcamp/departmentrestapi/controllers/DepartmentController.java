package com.devcamp.departmentrestapi.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.departmentrestapi.models.Department;
import com.devcamp.departmentrestapi.responsitory.IDepartmentResponsitory;

@RestController
@RequestMapping("/department")
@CrossOrigin(value = "*", maxAge = -1)
public class DepartmentController {
    @Autowired
    IDepartmentResponsitory pDepartmentResponsitory;

    // Create a department
    @PostMapping("/create")
    public ResponseEntity<Object> createDepartment(@Valid @RequestBody Department department) {
        try {
            Department savedDepartment = pDepartmentResponsitory.save(department);
            return new ResponseEntity<>(savedDepartment, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace(); 
            return ResponseEntity.unprocessableEntity().body("Failed to create the department: " + e.getMessage());
        }
    }

    //Update a department
    @PutMapping("update/{id}")
	public ResponseEntity<Object> updateDepartment(@Valid @PathVariable("id") Long id, @RequestBody Department department) {
		Optional<Department> departmentData = pDepartmentResponsitory.findById(id);
		if (departmentData.isPresent()) {
			Department newDepartment = departmentData.get();
			newDepartment.setDepartmentCode(department.getDepartmentCode());
			newDepartment.setDepartmentName(department.getDepartmentName());
			newDepartment.setBusiness(department.getBusiness());
            newDepartment.setIntroduction(department.getIntroduction());
            newDepartment.setEmployee(department.getEmployee());
			Department savedDepartment = pDepartmentResponsitory.save(newDepartment);
			return new ResponseEntity<>(savedDepartment, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    //Delete a department
    @DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteDepartmentById(@PathVariable Long id) {
		try {
			Optional<Department> optional= pDepartmentResponsitory.findById(id);
			if (optional.isPresent()) {
				pDepartmentResponsitory.deleteById(id);
			}else {
				//countryRepository.deleteById(id);
			}			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    //Get one department
    @GetMapping("/detail/{id}")
	public Department getDepartmentById(@PathVariable Long id) {
		if (pDepartmentResponsitory.findById(id).isPresent())
			return pDepartmentResponsitory.findById(id).get();
		else
			return null;
	}

    //Get all

	@GetMapping("/all")
	public List<Department> getAllCountry() {
		return pDepartmentResponsitory.findAll();
	}

}
