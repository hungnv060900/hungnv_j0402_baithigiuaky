package com.devcamp.departmentrestapi.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.departmentrestapi.models.Department;
import com.devcamp.departmentrestapi.models.Employee;
import com.devcamp.departmentrestapi.responsitory.IDepartmentResponsitory;
import com.devcamp.departmentrestapi.responsitory.IEmployeeResponsitory;

@RestController
@RequestMapping("/employee")
@CrossOrigin(value = "*", maxAge = -1)
public class EmployeeController {

    @Autowired
    IEmployeeResponsitory pEmployeeResponsitory;

    @Autowired
    IDepartmentResponsitory pDepartmentResponsitory;

    // Create a employee

    @PostMapping("create/{id}")
    public ResponseEntity<Object> createRegion(@Valid @PathVariable("id") Long id, @RequestBody Employee employee) {
        try {
            Optional<Department> departmentData = pDepartmentResponsitory.findById(id);
            if (departmentData.isPresent()) {
                Employee newRole = new Employee();
                newRole.setEmployeeCode(employee.getEmployeeCode());
                newRole.setEmployeeName(employee.getEmployeeName());
                newRole.setPosition(employee.getPosition());
                newRole.setGender(employee.getGender());
                newRole.setDateOfBirth(employee.getDateOfBirth());
                newRole.setAddress(employee.getAddress());
                newRole.setContactNumber(employee.getContactNumber());

                Department _department = departmentData.get();
                newRole.setDepartment(_department);

                Employee savedRole = pEmployeeResponsitory.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Update a employee
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateEmployee(@Valid @PathVariable("id") Long id, @RequestBody Employee employee) {
        Optional<Employee> employeeData = pEmployeeResponsitory.findById(id);
        if (employeeData.isPresent()) {
            Employee newEmployee = employeeData.get();
            newEmployee.setEmployeeCode(employee.getEmployeeCode());
            newEmployee.setEmployeeName(employee.getEmployeeName());
            newEmployee.setPosition(employee.getPosition());
            newEmployee.setGender(employee.getGender());
            newEmployee.setDateOfBirth(employee.getDateOfBirth());
            newEmployee.setAddress(employee.getAddress());
            newEmployee.setContactNumber(employee.getContactNumber());
            Employee savedEmployee = pEmployeeResponsitory.save(newEmployee);
            return new ResponseEntity<>(savedEmployee, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Delete a employee
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteEmployeeById(@PathVariable Long id) {
        try {
            pEmployeeResponsitory.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get one employee
    @GetMapping("/detail/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        if (pEmployeeResponsitory.findById(id).isPresent())
            return pEmployeeResponsitory.findById(id).get();
        else
            return null;
    }

    // Get all
    @GetMapping("/all")
    public List<Employee> getAllEmployee() {
        return pEmployeeResponsitory.findAll();
    }
}
