package com.devcamp.departmentrestapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.departmentrestapi.models.Employee;

public interface IEmployeeResponsitory extends JpaRepository<Employee,Long> {
    
}
