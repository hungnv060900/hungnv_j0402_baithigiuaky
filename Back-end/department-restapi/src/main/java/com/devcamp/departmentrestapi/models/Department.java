package com.devcamp.departmentrestapi.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Ma phong ban khong duoc de trong")
    @Size(min = 2, message = "Mã phong ban phải có ít nhất 2 ký tự ")
    @Column(name = "department_code",unique = true)
    private String departmentCode;

    @Column(name = "department_name")
    private String departmentName;

    @Column(name = "business")
    private String business;

    @Column(name = "introduction")
    private String introduction;

    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Employee> employee;

    public Department() {
    }

    public Department(Long id, @NotNull(message = "Ma phong ban khong duoc de trong") String departmentCode,
            String departmentName, String business, String introduction, Set<Employee> employee) {
        this.id = id;
        this.departmentCode = departmentCode;
        this.departmentName = departmentName;
        this.business = business;
        this.introduction = introduction;
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Set<Employee> getEmployee() {
        return employee;
    }

    public void setEmployee(Set<Employee> employee) {
        this.employee = employee;
    }

    

    
}
